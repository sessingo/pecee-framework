<?php

/**
 * Contain helper functions which provides shortcuts for various classes.
 */

function url($controller = NULL, $method = NULL, $methodParams = NULL, $getParams = NULL, $includeMethodParams = FALSE, $doRewrite = TRUE) {
    return \Pecee\Router::GetRoute($controller, $method, $methodParams, $getParams, $includeMethodParams, $doRewrite);
}

function redirect($url) {
    return \Pecee\Router::Redirect($url);
}

function lang($key, $args = null) {
    if (!is_array($args)) {
        $args = func_get_args();
        $args = array_slice($args, 1);
    }
    return \Pecee\Language::GetInstance()->_($key, $args);
}

/**
 * Adds flash message
 *
 * @param $message
 * @param $type
 * @param null $form
 * @param null $placement
 * @param null $index
 */
function message($message, $type, $form = NULL, $placement = NULL, $index = NULL) {
    $msg = new \Pecee\UI\Form\FormMessage();
    $msg->setMessage($message);
    $msg->setForm($form);
    $msg->setIndex($index);
    $msg->setPlacement($placement);

    \Pecee\Session\SessionMessage::GetInstance()->set($msg, $type);
}

/**
 * Get environment variable
 * @param $key
 * @param null $default
 *
 * @return null
 */
function env($key, $default = NULL) {
    $value = getenv($key);
    return (is_null($value)) ? $default : $value;
}