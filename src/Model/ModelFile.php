<?php
namespace Pecee\Model;
use Pecee\DB\DBTable;
use Pecee\File;

class ModelFile extends \Pecee\Model\ModelData {
	const ORDER_DATE_ASC = 'f.`createdDate` ASC';
	const ORDER_DATE_DESC = 'f.`createdDate` DESC';

	public static $ORDERS = array(self::ORDER_DATE_ASC, self::ORDER_DATE_DESC);

	public function __construct($name = NULL, $path = NULL) {
		$fullPath=NULL;
		if(!is_null($name) && !is_null($path)) {
			$fullPath=$path.(File::HasEndingDirectorySeperator($path) ? '' : DIRECTORY_SEPARATOR) . $name;
		}

        $table = new DBTable();
        $table->column('fileId')->string(40)->primary();
        $table->column('filename')->string(355)->index();
        $table->column('originalFilename')->string(355)->index();
        $table->column('path')->string(355)->index();
        $table->column('type')->string(255)->index();
        $table->column('bytes')->integer()->index();
        $table->column('createdDate')->datetime()->index();

		parent::__construct($table);

        $this->fileId = \Pecee\Guid::Create();
        $this->filename = $name;
        $this->path = $path;

        if($fullPath && is_file($fullPath)) {
            $this->type = File::GetMimeType($fullPath);
            $this->bytes = filesize($fullPath);
        }

        $this->createdDate = \Pecee\Date::ToDateTime();
	}

	public function setFilename($filename) {
		$this->filename = $filename;
	}

	public function setOriginalFilename($filename) {
		$this->originalFilename = $filename;
	}

	public function setPath($path) {
		$this->path = $path;
	}

	public function setType($type) {
		$this->type = $type;
	}

	public function setBytes($bytes) {
		$this->bytes = $bytes;
	}

	public function setCreatedDate($datetime) {
		$this->createdDate = $datetime;
	}

	public function updateData() {
		if($this->data) {
			/* Remove all fields */
			ModelFileData::RemoveAll($this->fileId);
			foreach($this->data->getData() as $key=>$value) {
				$data=new ModelFileData($this->fileId, $key, $value);
				$data->save();
			}
		}
	}

	protected function fetchData($row) {
		$data = ModelFileData::GetFileId($row->fileId);
		if($data->hasRows()) {
			foreach($data->getRows() as $d) {
				$row->setDataValue($d->getKey(), $d->getValue());
			}
		}
	}

	public function getFullPath() {
		return $this->path . (File::HasEndingDirectorySeperator($this->path) ? '' : DIRECTORY_SEPARATOR) . $this->Filename;
	}

	/**
	 * Get file by file id.
	 * @param string $fileId
	 * @return self
	 */
	public static function GetById($fileId){
		return self::FetchOne('SELECT * FROM {table} WHERE `fileId` = %s', array($fileId));
	}

	public static function Get($order=NULL, $rows=NULL, $page=NULL){
		$order = (in_array($order, self::$ORDERS)) ? $order : self::ORDER_DATE_DESC;
		return self::FetchPage('SELECT f.* FROM {table} f ORDER BY ' .$order, $rows=NULL,$page=NULL);
	}
}