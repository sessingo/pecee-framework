<?php
namespace Pecee\Model;
use Pecee\Date;
use Pecee\DB\DBTable;

class ModelSession extends Model {
	public function __construct($name = NULL, $value = NULL) {
        $table = new DBTable();
        $table->column('name')->string()->primary();
        $table->column('value')->longtext();
        $table->column('time')->datetime()->index();

		parent::__construct($table);

        $this->name = $name;
        $this->value = $value;
	}

	public function save() {
		self::NonQuery('DELETE FROM {table} WHERE `Time` <= %s', Date::ToDateTime(time()-(60*30)));

		$session = $this->Get($this->Name);
		if($session->hasRows()) {
			$session->Time = Date::ToDateTime();
			$session->update();
		} else {
			parent::save();
		}
	}

	/**
	 * Get Session by key
	 * @param string $key
	 * @return \Pecee\Model\ModelSession
	 */
	public static function Get($key) {
		return self::FetchOne('SELECT * FROM {table} WHERE `Name` = %s', $key);
	}
}