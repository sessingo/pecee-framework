<?php
namespace Pecee;
use Pecee\Handler\ExceptionHandler;
use Pecee\Router\RouterAlias;
use Pecee\Router\RouterException;

class Router {
	private static $instance;
	protected $originalPath;
	protected $path;
	protected $controller;
	protected $originalController;
	protected $method;
	protected $originalMethod;
	protected $params;
	protected $alias;
	protected $exceptionHandlers;

	const CONTROLLER_DEFAULT = 'Default';
	const CONTROLLER_METHOD_PREFIX = 'view';
	const METHOD_DEFAULT = 'index';
	const METHOD_CALLACTION = 'callAction';

	const SETTINGS_APPNAME = 'AppName';

	public static $ALLOWED_PREFIXES=array('json','ajax');

	/**
	 * Gets new instance
	 * @return self
	 */
	public static function GetInstance() {
		if(is_null(self::$instance)) {
			$caller=get_called_class();
			self::$instance=new $caller();
		}
		return self::$instance;
	}

	public function __construct() {
		Debug::GetInstance()->add('Router initialised.');
		$this->alias = array();
		$this->exceptionHandlers = array();
		$this->params = array();
		Locale::GetInstance(); // Init locale settings
	}

	public static function GoBack($return = false) {
		$ref = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : NULL;
		if(!empty($ref)) {
			if($return){
				return $ref;
			}
			self::Redirect($ref);
		} else {
			if($return){
				return NULL;
			}
			self::Redirect(self::GetRoute('', ''));
		}
		die();
	}

	public static function Refresh() {
		self::Redirect(self::GetCurrentRoute());
	}

	public static function Redirect($url) {
		header('location: '.$url);
		die();
	}

	/**
	 * Calculates template path from given Widget name.
	 *
	 * @param string $name
	 * @return string
	 */
	public static function GetTemplatePath($name) {
		$path=explode('\\', $name);
		$path = array_slice($path, 2);
		return 'Template' . DIRECTORY_SEPARATOR . 'Content' . DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, $path) . '.php';
	}

	public static function GetRoute($controller = NULL, $method = NULL, $methodParams = NULL, $getParams = NULL, $includeMethodParams = FALSE, $doRewrite = TRUE) {
		return self::GetInstance()->calculateRoute($controller, $method, $methodParams, $getParams, $includeMethodParams, $doRewrite);
	}

	public static function LoadClass($class) {
		return class_exists($class) ? new $class() : NULL;
	}

	public function getPageName() {
		return join($this->originalPath, '_');
	}

	public function addAlias(RouterAlias $alias) {
		$this->alias[] = $alias;
	}

	public function hasAlias() {
		return ($this->alias && count($this->alias)>0);
	}

	public function getAlias() {
		return $this->alias;
	}

	public function addExceptionHandler(ExceptionHandler $handler) {
		$this->exceptionHandlers[] = $handler;
	}

	public function hasExceptionHandler() {
		return ($this->exceptionHandlers && count($this->exceptionHandlers) > 0);
	}

	public function getExceptionHandlers() {
		return $this->exceptionHandlers;
	}

	public function getPath($return=false) {
		return ($return) ? strtolower($this->getController() . (($this->getMethod()) ? '_' . $this->getMethod() : '')) : $this->path;
	}

	public function getController() {
		return $this->controller;
	}

	public function getOriginalController() {
		return $this->originalController;
	}

	public function getParams() {
		return $this->params;
	}

	public function getMethod() {
		return $this->method;
	}

	public function getOriginalMethod() {
		return $this->originalMethod;
	}

	protected function calculatePath() {
		$this->controller = (isset($this->path[0]) && !empty($this->path[0])) ? ucfirst($this->path[0]) : self::CONTROLLER_DEFAULT;
		$this->method = (isset($this->path[1])) ? str_replace(array('-'), '', $this->path[1]) : self::METHOD_DEFAULT;
	}

	public function routeRequest() {
		try {
			$path = ltrim( substr( $_SERVER['REQUEST_URI'], strlen( dirname( $_SERVER['SCRIPT_NAME'] ) ) ), '/' );
			Debug::GetInstance()->add( 'Router request: "' . $path . '"' );
			$path = current( explode( '?', $path ) );
			$this->path = explode( '/', trim( $path, '/' ) );

			$p = $this->path[ count( $this->path ) - 1 ];
			$prefix = self::CONTROLLER_METHOD_PREFIX;
			if (strrpos( $p, '.' ) !== false) {
				$p = substr($p, strrpos( $p, '.' ) + 1);
				if (in_array( strtolower( $p ), self::$ALLOWED_PREFIXES )) {
					$p = str_replace( '-', '', $p );
					$prefix = ucfirst( self::$ALLOWED_PREFIXES[ array_search( $p, self::$ALLOWED_PREFIXES ) ] );
					$this->path[ count( $this->path ) - 1 ] = substr( $this->path[ count( $this->path ) - 1 ], 0, ( strripos( $this->path[ count( $this->path ) - 1 ], $p ) - 1 ) );
				}
			}

			$this->calculatePath();
			$path = explode('/', trim( $path, '/'));

			if ($this->hasAlias()) {
				/* @var $alias \Pecee\Router\RouterAlias */
				foreach ($this->alias as $alias) {
					$path = $alias->getPath( join( $this->path, '/' ) );
				}
				$path = explode( '/', trim( $path, '/' ) );
				$this->originalPath = $this->path;
				$this->path = $path;
			}

			if (is_array($path)) {
				$path = array_slice($path, 2);
			}

			$this->params = $path;
			$this->originalController = $this->controller;
			$this->originalMethod = $this->method;
			$this->calculatePath();
			$method  = $this->method . $prefix;

			$appname = Registry::GetInstance()->get(self::SETTINGS_APPNAME, false);

			if (!$appname) {
				throw new RouterException('"AppName" registry key not defined!', 2);
			}

			$classname = $appname . '\\Controller\\Controller' . $this->controller;
			if (!class_exists($classname)) {
				$classname = 'Pecee\Controller\Controller' . $this->controller;
			}

			if (!class_exists($classname)) {
				throw new RouterException(sprintf('Controller "%s" doesn\'t exist.', $classname), 404);
			}

			$class = $this->LoadClass($classname);

			Debug::GetInstance()->add( 'Invoking callmethod: ' . self::METHOD_CALLACTION );
			call_user_func_array(array($class, self::METHOD_CALLACTION), array_merge(array($this->method), $this->params));

			if (!method_exists($class, $method)) {
				throw new RouterException(sprintf('The method "%s" is not yet implementet in "%s".', $method, $classname), 404);
			}
			$reflection = new \ReflectionMethod($class, $method);
			if ($reflection->getNumberOfParameters() > 0) {
				$requireArguments = array();
				/* @var $param \ReflectionParameter */
				foreach ($reflection->getParameters() as $key => $param) {
					if (!$param->isOptional()) {
						$requireArguments[] = $param->getName();
					}
				}
				foreach ($requireArguments as $key => $arg) {
					if (!isset($this->params[$key])) {
						throw new RouterException(sprintf('Missing required argument "%s" for method "%s" in controller "%s".', $arg, $this->originalMethod, $this->originalController), 404);
					}
				}
			}
			Debug::GetInstance()->add('Routing request to class: ' . $classname . '(' . join( ',', $this->params ) . ')');

			return call_user_func_array(array($class, $method), $this->params);
		} catch(\Exception $ex) {

			/* @var $e ExceptionHandler */
			foreach($this->exceptionHandlers as $e) {
				$e->handleError($ex);
			}

			if(isset($class)) {
				// Run destructor before dying
				$class->destruct();
			}

			throw $ex;
		}
	}

	public function calculateRoute($controller = NULL, $method = NULL, $methodParams = NULL, $getParams = NULL, $includeMethodParams = FALSE, $doRewrite = TRUE) {
		if(!is_null($methodParams) && !is_array($methodParams)) {
			throw new \InvalidArgumentException('Method params must be NULL or an array.');
		}
		if(!is_null($getParams) && !is_array($getParams)) {
			throw new \InvalidArgumentException('Get params must be an array. For example: array(key => value).');
		}

		if(is_null($controller) && is_null($method) && is_null($methodParams) && !$includeMethodParams) {
			$path = self::GetCurrentRoute(TRUE, FALSE);
			$doRewrite = FALSE;
		} else {
			$path = '/';
			$tmpPath = array();
			if(is_null($controller)) {
				$tmpPath[] = strtolower($this->controller);
			} elseif(!empty($controller)) {
				$tmpPath[] = $controller;
			}
			if(is_null($method)) {
				$tmpPath[] = strtolower($this->method);
			} elseif(!empty($method)) {
				$tmpPath[] = $method;
			}
			if(count($tmpPath) > 0) {
				$path .= join('/', $tmpPath) . '/';
			}
			if($includeMethodParams) {
				$originalMethodParams=$methodParams;
				if(count($this->params) > 0 && strtolower($this->params[0]) == strtolower($this->originalMethod) ) {
					$methodParams=array_slice($this->path, 3);
				} else {
					$index=(isset($this->path[0]) && $this->path[0]==strtolower($this->controller)) ? 1 : 0;
					$index=(isset($this->path[1]) && $this->path[1]==strtolower($this->method) && is_null($method) || isset($this->path[1]) && $method == $this->path[1]) ? ($index+1) : $index;
					if(!is_null($method) && count($this->path) > 0) {
						$index+=array_search($this->originalMethod, $this->path);
					}
					$methodParams=array_slice($this->path, $index);
				}
				if(is_array($originalMethodParams)) {
					$methodParams=array_merge($methodParams, $originalMethodParams);
				}
				$getParams=is_null($getParams) ? $_GET : array_merge($_GET, $getParams);
			}

			if(count($methodParams) > 0) {
				$methodParams = ArrayUtil::Filter($methodParams,FALSE);
				if(count($methodParams) > 0) {
					$path .= join('/', $methodParams) . '/';
				}
			}
		}
		if(is_array($getParams)) {
			$getParams = ArrayUtil::Filter($getParams);
			if(count($getParams) > 0) {
				$path .= '?';
			}
			$path .= Url::QueryStringToString($getParams);
		}

		if($doRewrite && $this->hasAlias()) {
			/* @var $alias \Pecee\Router\RouterAlias */
			foreach($this->alias as $alias) {
				$path = $alias->getUrl($path);
			}
		}
		return $path;
	}

	public static function GetCurrentRoute($relative=TRUE, $includeParams = TRUE) {
		return Url::CurrentPageUrl($relative, $includeParams);
	}
}