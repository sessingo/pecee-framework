<?php
namespace Pecee\Controller;
use Pecee\Controller;
use Pecee\Session;
use Pecee\UI\Form\FormCaptcha;

class ControllerCaptcha extends Controller {
	public function showView($captchaName) {
        $session = Session::GetInstance();
		if($session->exists($captchaName . '_data')) {
			$captcha = $session->get($captchaName . '_data');
			if($captcha instanceof FormCaptcha) {
				$captcha->showCaptcha();
                $session->destroy($captchaName . '_data');
			}
		}
	}
}