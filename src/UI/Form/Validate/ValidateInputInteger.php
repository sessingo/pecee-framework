<?php
namespace Pecee\UI\Form\Validate;
use Pecee\Integer;

class ValidateInputInteger extends ValidateInput {

	protected $allowEmpty;
	public function __construct($allowEmpty=FALSE) {
		$this->allowEmpty=$allowEmpty;
	}

	public function validate() {
		return ($this->allowEmpty && empty($this->value) || Integer::is_int($this->value));
	}
	public function getErrorMessage() {
		return lang('%s is not a valid number', $this->name);
	}
}