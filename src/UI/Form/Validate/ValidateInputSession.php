<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputSession extends ValidateInput {
	protected $sessionName;
	public function __construct( $sessionName ) {
		$this->sessionName = $sessionName;
	}
	public function validate() {
		return ((bool)\Pecee\Session::GetInstance()->exists($this->sessionName));
	}
	public function getErrorMessage() {
		return lang('%s does not exist', $this->name);
	}
}