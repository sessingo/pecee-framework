<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputUri extends ValidateInput {
	protected $error;
	public function validate() {
		if(empty($this->value)) {
			$this->error = lang('%s is required', $this->name);
			return FALSE;
		} elseif(!\Pecee\Url::IsValid($this->value)) {
			$this->error = lang('%s is not a valid link', $this->name);
			return FALSE;
		}
		return TRUE;
	}
	public function getErrorMessage() {
		return $this->error;
	}
}