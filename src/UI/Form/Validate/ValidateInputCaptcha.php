<?php
namespace Pecee\UI\Form\Validate;
class ValidateInputCaptcha extends ValidateInput {
	protected $captchaName;
	public function __construct($name) {
		$this->captchaName = $name;
	}
	public function validate() {
		$result = (\Pecee\Session::GetInstance()->exists($this->captchaName) && strtolower($this->value) == strtolower(\Pecee\Session::GetInstance()->get($this->captchaName)));
		if($result) {
			\Pecee\Session::GetInstance()->destroy($this->captchaName);
		}
		return $result;
	}
	public function getErrorMessage() {
		return lang('Invalid captcha verification');
	}
}