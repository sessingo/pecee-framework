<?php
namespace Pecee\UI\Html;
class HtmlSelectOption extends \Pecee\UI\Html\Html {
	public function __construct($name, $value, $selected=FALSE) {
		parent::__construct('option', $value);
		$this->addAttribute('value',$value);
		if($selected) {
			$this->addAttribute('selected', 'selected');
		}
		$this->setInnerHtml($name);
	}
}