<?php
namespace Pecee;
abstract class Dataset {
	protected $data;
	public function getArray() {
		return $this->data;
	}
	public function search($query) {
		$result = array();
		$words = explode(' ', $query);
		foreach($words as $key=>$word) {
			if($word == $query)
				$result[$key] = $word;
		}
		$this->data = $result;
		return $this->data;
	}
	protected function createArray($value=NULL, $name) {
		$arr = array();
		if(!is_null($value)) {
			$arr['value'] = htmlspecialchars($value);
		}
		$arr['name'] = $name;
		$this->data[] = $arr;
	}
}