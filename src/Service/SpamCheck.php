<?php
namespace Pecee\Service;
class Spamcheck {
	const SERVICE_URI='http://master.moinmo.in/BadContent?action=raw';
	const FILENAME='spamlist.txt';
	protected $text;
	protected $list;
	protected $pathToSpamlist;
	public function __construct($text) {
		$this->text=$text;
	}
	
	protected function getList() {
		/*$file=sys_get_temp_dir() . DIRECTORY_SEPARATOR . self::FILENAME;
		if(!file_exists($file) || file_exists($file) && (time()-filemtime($file)) > 60*60*24) {
			$this->list=file_get_contents(self::SERVICE_URI);
			file_put_contents($file, $this->list);
		} else {
			$this->list=file_get_contents($file);
		}*/
		if(!$this->list) {
			if(!file_exists($this->pathToSpamlist)) {
				throw new \ErrorException('Spamlist file doesn\'t exist');
			}
			$this->list=file_get_contents($this->pathToSpamlist, FILE_USE_INCLUDE_PATH);
			$this->list=explode(chr(10), $this->list);
		}
	}
	
	public function isSpam() {
		$this->getList();
		if(count($this->list) > 0) {
			$ignoreTags=array('#');
			foreach($this->list as $regex) {
				$regex=trim($regex);
				if(!in_array(substr($regex, 0, 1), $ignoreTags) && !empty($regex) && strlen($regex) > 3) {
					if(preg_match('/'.preg_quote($regex, '/').'/is', $this->text)) {
						return TRUE;
					}
				}
			}
		}
		return FALSE;
	}
	
	public function setPathToSpamList($path) {
		$this->pathToSpamlist=$path;
	}
	
	public function setList($spamList) {
		$this->list=$spamList;
	}
}