<?php
namespace Pecee\Service\Memcache;
use Pecee\Registry;

class Memcache {
	const SETTINGS_HOST = 'MEMCACHE_HOST';
	const SETTINGS_PORT = 'MEMCACHE_PORT';
	private static $instance;
	protected $memcache;
	public static function GetInstance() {
		if(!self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	public function __construct() {
		$this->memcache = new \Memcached();
        if(!$this->memcache->addServer(Registry::GetInstance()->get(self::SETTINGS_HOST, '127.0.0.1'), Registry::GetInstance()->get(self::SETTINGS_PORT, 11211))) {
		    throw new MemcacheException("Error connecting to memcache server: " . \Pecee\Registry::GetInstance()->get(self::SETTINGS_HOST, '127.0.0.1'));
        }
	}
	public function set($key, $value, $expire) {
		$this->memcache->set($key, $value, $expire);
	}
	public function get($key) {
		return $this->memcache->get($key);
	}
	public function clear($key=NULL) {
		if(!is_null($key)) {
			$this->memcache->delete($key);
		} else {
			$this->memcache->flush();
		}
	}
	public function getMemcache() {
		return $this->memcache;
	}
	public function close() {
		$this->memcache->quit();
	}
}